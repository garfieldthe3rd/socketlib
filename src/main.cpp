#include <iostream>
#include "socket/endpoints.hpp"

int main() {
	WSADATA wsaData;

	// Initialize Winsock
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cerr << "Startup failed!" << std::endl;
		return 1;
	}

	socketlib::endpoint<socketlib::tcp_v4> a("localhost", 12345);
	socketlib::endpoint<socketlib::tcp> b(12345);

	std::cout << a.address() << std::endl;

	WSACleanup();

	std::cin.get();

	return 0;
}