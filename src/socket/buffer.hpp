#pragma once

namespace socketlib {

	template < class T >
	class buffer {
	public:
		using data_type = T;

	private:
		data_type *m_data;
		size_t m_size;

	public:
		buffer(data_type *data, size_t size) : m_data(data), m_size(size) {
		}

		template < size_t N >
		buffer(data_type(&data)[N]) : m_data(data), m_size(N) {
		}

		buffer(std::vector<T> &data) : m_data(data.data()), m_size(data.size()) {
		}

		data_type *data() {
			return m_data;
		}

		const data_type *data() const {
			return m_data;
		}

		size_t size() const {
			return m_size;
		}
	};

	template < class T >
	class const_buffer {
	public:
		using data_type = T;

	private:
		copnst data_type *m_data;
		size_t m_size;

	public:
		const_buffer(const data_type *data, size_t size) : m_data(data), m_size(size) {
		}

		template < size_t N >
		const_buffer(const data_type (&data)[N]) : m_data(data), m_size(N) {
		}

		const_buffer(const std::vector<T> &data) : m_data(data.data()), m_size(data.size()) {
		}

		const data_type *data() const {
			return m_data;
		}

		size_t size() const {
			return m_size;
		}
	};

} // namespace socketlib