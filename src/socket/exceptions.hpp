#pragma once

#include <string>
#include <stdexcept>

namespace socketlib {

	class socket_exception : public std::runtime_error {
	public:
		socket_exception(const std::string &msg) : std::runtime_error(msg) {
		}
	};

	class endpoint_exception : public std::runtime_error {
	public:
		endpoint_exception(const std::string &msg) : std::runtime_error(msg) {
		}
	};

} // namespace socketlib