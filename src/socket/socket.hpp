#pragma once

#include <winsock.h>
#include <ws2tcpip.h>
#include "exceptions.hpp"

namespace socketlib {

	class ipv4_endpoint {
	public:
		using endpoint_type = sockaddr_in;

	private:
		endpoint_type m_name;

	public:
		ipv4_endpoint(unsigned long address, unsigned short port) : m_name{ AF_INET, port} {
			m_name.sin_addr.S_un.S_addr = address;
		}

		ipv4_endpoint(const std::string& address, unsigned short port) : m_name{} {
			addrinfo hints = { NULL, AF_INET, NULL, NULL, 0, nullptr, nullptr, nullptr };
			addrinfo *result = nullptr;

			if (::getaddrinfo(address.c_str(), std::to_string(port).c_str(), &hints, &result) != 0) {
				throw failed_name_resolution_exception(address);
			}
			m_name = *reinterpret_cast<endpoint_type*>(result->ai_addr);

			freeaddrinfo(result);
		}

		ipv4_endpoint(const ipv4_endpoint &) = default;
		ipv4_endpoint(ipv4_endpoint &&) = default;
		~ipv4_endpoint() = default;

		ipv4_endpoint& operator=(const ipv4_endpoint &) = default;
		ipv4_endpoint& operator=(ipv4_endpoint &&) = default;

		size_t size() const {
			return sizeof(endpoint_type);
		}

		const endpoint_type *data() const {
			return &m_name;
		}

		bool operator==(const ipv4_endpoint &endpoint) const {
			return std::memcmp(&m_name, &endpoint.m_name, sizeof(sockaddr));
		}

		bool operator!=(const ipv4_endpoint &endpoint) const {
			return !this->operator==(endpoint);
		}
	};

	struct tcp_ipv4_protocol {
		using endpoint = ipv4_endpoint;
		static constexpr int family = AF_INET;
		static constexpr int type = SOCK_STREAM;
		static constexpr int protocol = IPPROTO_TCP;
	};

	template < typename P >
	class socket {
	public:

	private:
		using socket_type = SOCKET;
		using protocol = P;

		socket_type handle;

	public:
		socket() : handle(::socket(protocol::family, protocol::type, protocol::protocol)) {
			if (handle == INVALID_SOCKET) {
				throw socket_creation_exception();
			}
		}

		socket(ipv4_endpoint endpoint) : socket() {
			if (::bind(handle, endpoint.data(), endpoint.size()) != 0) {
				throw socket_binding_exception();
			}
		}

		socket(const socket&) = delete;
		socket(socket &&socket) : handle(socket.handle) {
			socket.handle = INVALID_SOCKET;
		}

		~socket() {
			if (handle != INVALID_SOCKET) {
				::closesocket(handle);
			}
		}

		socket &operator=(const socket&) = delete;
		socket &operator=(socket&& socket) {
			std::swap(handle, socket.handle);

			return *this;
		}


	};

} // namespace socketlib