#pragma once

#include <WinSock2.h>

namespace socketlib {

	enum class ip_family {
		IP_V4 = AF_INET,
		IP_V6 = AF_INET6,
		IP_UNSPEC = AF_UNSPEC
	};

	enum class socket_type {
		STREAM = SOCK_STREAM,
		DATAGRAM = SOCK_DGRAM,
		RAW = SOCK_RAW
	};

	enum class protocol_type {
		TCP = ::IPPROTO_TCP,
		UDP = ::IPPROTO_UDP
	};

	template < ip_family F, socket_type S, protocol_type P >
	class ip_protocol {
	public:
		static constexpr ip_family family = F;
		static constexpr socket_type type = S;
		static constexpr protocol_type protocol = P;

		ip_protocol() = delete;
	};

	using tcp = ip_protocol<ip_family::IP_UNSPEC, socket_type::STREAM, protocol_type::TCP>;
	using tcp_v4 = ip_protocol<ip_family::IP_V4, socket_type::STREAM, protocol_type::TCP>;
	using tcp_v6 = ip_protocol<ip_family::IP_V6, socket_type::STREAM, protocol_type::TCP>;

	using udp = ip_protocol<ip_family::IP_UNSPEC, socket_type::DATAGRAM, protocol_type::UDP>;
	using udp_v4 = ip_protocol<ip_family::IP_V4, socket_type::DATAGRAM, protocol_type::UDP>;
	using udp_v6 = ip_protocol<ip_family::IP_V4, socket_type::DATAGRAM, protocol_type::UDP>;

} // namespace socketlib