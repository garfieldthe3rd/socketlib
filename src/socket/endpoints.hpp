#pragma once

#include <string>
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>
#include "exceptions.hpp"
#include "protocols.hpp"

namespace socketlib {

	template < class P >
	class endpoint {
	public:
		using protocol_type = P;

	private:
		union {
			sockaddr base;
			sockaddr_in v4;
			sockaddr_in6 v6;
		} m_addr;

	public:
		endpoint(unsigned short port) : m_addr() {
			switch (protocol_type::family) {
			case ip_family::IP_V4:
				m_addr.v4 = { AF_INET, ::htons(port), INADDR_ANY,{ 0 } };
				break;
			case ip_family::IP_V6:
				m_addr.v6 = { AF_INET6, ::htons(port), 0, INADDR_ANY, 0 };
				break;
			case ip_family::IP_UNSPEC:
				m_addr.v4 = { AF_UNSPEC, ::htons(port), INADDR_ANY,{ 0 } };
				break;
			}
		}
		
		endpoint(const std::string &address, unsigned short port) : m_addr() {
			addrinfo hints{0, static_cast<int>(protocol_type::family), static_cast<int>(protocol_type::type),
							static_cast<int>(protocol_type::protocol), 0, nullptr, nullptr, nullptr};
			addrinfo *result = nullptr;

			if (::getaddrinfo(address.c_str(), std::to_string(port).c_str(), &hints, &result) != 0) {
				freeaddrinfo(result);
				throw endpoint_exception("Failed to resolve address!");
			}

			m_addr.base = *result->ai_addr;
			freeaddrinfo(result);
		}

		bool operator==(const endpoint<P>& e) const {
			return std::memcmp(&m_addr.base, &e.m_addr.base, size());
		}

		bool operator!=(const endpoint<P>& e) const {
			return !this->operator==(e);
		}

		std::string address() const {
			return std::to_string(m_addr.v4.sin_addr.S_un.S_un_b.s_b1) + "." +
				std::to_string(m_addr.v4.sin_addr.S_un.S_un_b.s_b2) + "." +
				std::to_string(m_addr.v4.sin_addr.S_un.S_un_b.s_b3) + "." +
				std::to_string(m_addr.v4.sin_addr.S_un.S_un_b.s_b4);
		}
	};

} // namespace socketlib