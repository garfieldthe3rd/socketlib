CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

PROJECT(SocketTest)

if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	MESSAGE(STATUS "64-bit compiler detected!")
else(CMAKE_SIZEOF_VOID_P EQUAL 8)
	MESSAGE(STATUS "32-bit compiler detected!")
endif(CMAKE_SIZEOF_VOID_P EQUAL 8)

if("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")
   message(WARNING "In-source building is discouraged!")
endif("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")

ADD_EXECUTABLE(
	SocketTest
	src/socket/buffer.hpp
	src/socket/endpoints.hpp
	src/socket/exceptions.hpp
	src/socket/protocols.hpp
	src/socket/socket.hpp
	src/main.cpp
)

if(WIN32)
	target_link_libraries(SocketTest wsock32 ws2_32)
endif()

set_target_properties(SocketTest PROPERTIES LINKER_LANGUAGE CXX)
set_property(TARGET SocketTest PROPERTY CXX_STANDARD 17)
set_property(TARGET SocketTest PROPERTY CXX_STANDARD_REQUIRED ON)

add_custom_target(run
    COMMAND SocketTest
    DEPENDS SocketTest
    WORKING_DIRECTORY ${CMAKE_PROJECT_DIR}
)